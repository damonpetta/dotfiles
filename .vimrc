set nocompatible
filetype off

set rtp+=~/.vim/bundle/Vundle.vim          " for NeoVim
call vundle#rc('~/.vim/bundle')            " for NeoVim

call vundle#begin()
  Plugin 'airblade/vim-gitgutter'
  Plugin 'neomake/neomake'                 " lint on save
  Plugin 'ConradIrwin/vim-bracketed-paste' " auto :set paste
  Plugin 'flazz/vim-colorschemes'
  Plugin 'freeo/vim-kalisi'
  Plugin 'godlygeek/tabular'
  Plugin 'kien/ctrlp.vim'
  Plugin 'mileszs/ack.vim'                 " search across files
  Plugin 'nanotech/jellybeans.vim'         " fancy colors
  Plugin 'Raimondi/delimitMate'            " auto brackets
  Plugin 'scrooloose/syntastic'
  Plugin 'tpope/vim-commentary'
  Plugin 'tpope/vim-fugitive'
  Plugin 'tpope/vim-markdown'
  Plugin 'tpope/vim-repeat'                " repeating for change around
  Plugin 'tpope/vim-surround'
  Plugin 'vim-pandoc/vim-pandoc'
  Plugin 'vim-pandoc/vim-pandoc-syntax'
  Plugin 'ujihisa/neco-look'
  Plugin 'reedes/vim-lexical'
  Plugin 'panozzaj/vim-autocorrect'
  Plugin 'VundleVim/Vundle.vim'
  Plugin 'Valloric/YouCompleteMe'
  Bundle 'powerline/powerline',     {'rtp': 'powerline/bindings/vim/'}
call vundle#end()

filetype plugin indent on

syntax on
set mouse=r
scriptencoding utf-8
set shortmess+=filmnrxoOtT
set viewoptions=folds,options,cursor,unix,slash
set virtualedit=onemore
set history=2500
set showmode
set backspace=indent,eol,start
set linespace=0
set showmatch
set list
set listchars=tab:›\ ,trail:•,extends:#,nbsp:.
set wildmenu
set ignorecase
set smartcase
set hlsearch
set nowrap
set shiftwidth=2
set expandtab
set tabstop=2
set softtabstop=2
set nojoinspaces
set splitright
set splitbelow
set laststatus=2
set formatoptions-=cro

" Color scheme
colorscheme darkburn
" colorscheme jellybeans

set background=dark
highlight NonText ctermfg=bg guifg=bg
set t_Co=256

setlocal fo+=aw "Fix ugly line breaks http://wcm1.web.rice.edu/mutt-tips.html


autocmd BufNewFile,BufReadPost *.md set filetype=markdown

augroup lexical
  autocmd!
  autocmd FileType markdown,mkd call lexical#init()
  autocmd FileType textile call lexical#init()
  autocmd FileType text call lexical#init({ 'spell': 0 })
augroup END

augroup autocorrrect
  autocmd!
  autocmd FileType markdown,mkd call AutoCorrect()
  autocmd FileType text call AutoCorrect()
augroup END

" Turn off folding for vim-pandoc
let g:pandoc#modules#disabled = ["folding"]

" Lint on save
call neomake#configure#automake('w')

